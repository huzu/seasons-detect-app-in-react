import React from 'react';
import ReactDOM from 'react-dom';

// Class Based Component
class App extends React.Component{
	// Behaves same as if defined in a constructor. 
	state = {lat: null, errorMessage: null};

	// Removing constructor as babel automatically generates a constructor and initializes state as mentioned above. 
	// constructor(props){
	// 	super(props);

	// 	// This is the only time we do direct assignment to this.state !!!
	// 	this.state = {lat: null, errorMessage: null};
	// }

	// Lifecycle methods
	componentDidMount(){
		console.log("did mount callled.....");
		window.navigator.geolocation.getCurrentPosition(
			position => this.setState({lat: position.coords.latitude}),
			err => this.setState({errorMessage: err.message})
		);
	}

	// componentDidUpdate(){
	// 	console.log("did update callled.....");
	// }

	// React says we have to define render. 
	render(){
		if (this.state.errorMessage && !this.state.lat) {
			return <div>Error: {this.state.errorMessage}</div>;
		}
		
		if (!this.state.errorMessage && this.state.lat) {
			return <div>Latitude: {this.state.lat}</div>;
		}
		return <div>Loading !!!</div>;
	}
}
ReactDOM.render(<App />, document.querySelector('#root'));