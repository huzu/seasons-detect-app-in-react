import './SeasonDisplay.css';
import React from 'react';


const getSeason = (lat, month) => {
	if (month > 2 && month < 9){
		return (lat > 0 ? "summer" : "winter");
	} else {
		return (lat > 0 ? "winter" : "summer")
	}
}

const seasonConfig = {
	summer: {
		text: "Let's hit the beach !",
		iconName: 'sun'
	},
	winter: {
		text: 'Burr, it is cold !',
		iconName: 'snowflake'
	}
};

const SeasonDisplay = props => {
	const season = getSeason(props.lat, new Date().getMonth());
	const {text, iconName} = seasonConfig[season];	
	// No need to write repetetive ternery conditions instead we can directly create a season config object which can be used at all respective places.
	// const text_to_display = (season === 'winter' ? 'Burr, it is chilly' : 'Lets hit the beach');
	// const icon = (season === 'winter' ? 'snowflake' : 'sun')

	return (
		<div className={`season-display ${season}`}>
			<i className={`icon-left massive ${iconName} icon`}></i>
			<h1>{text}</h1>
			<i className={`icon-right massive ${iconName} icon`}></i>
		</div>
	);
}
export default SeasonDisplay;