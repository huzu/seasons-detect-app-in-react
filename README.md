# Season Detect Application Using React

1. This application is created using another form of React Components which is called as      class component. 
2. There are some rules for class level components which are: 
		a. It must be a javascript class
		b. It must extend React.Component
		c. It must define a `render` method that returns some amount of JSX.
3. Class components has `state` which is very crucial in it's lifecycle. 
4. `state` is a JS object that contains data relevant to a component. 
5. Updating `state` on a component causes the component to instantly rerender the page.       Which means that whenever a state changes, the component is reloaded and returns the       updated JSX. 
6. State must be initialized when the component is created. 
7. State can be updated using the function `setState`
8. The call to `super` method inside the `constructor` means that we still want to execute    all the functions return in the original `constructor` method under `React.Component`      class.
9. React provides a way to set default values to props in case of no value is passed from 	 the parent component which is called as `defaultProps`. We can define the default 				 values of props in this hash.

## Component Lifecycle

1. Constructor - When a new instance of our component is created, constructor is called. 
2. Render Method - render method is not optional. render method will be called at some 			 time in the lifecycle and returns JSX.
3. componentDidMount event - called when our component loads up initially. Good place to 		 do data loading as it gets invoked only 1 time. 
4. componentDidUpdate event - anytime our component gets updated through setState, it gets 	 triggered. it will sit and wait for any updates to occur on the component.  
5. componentWillUnmount event - used to clean up some component. 	
	 There are other 3 lifecycle methods too which are `shouldComponentUpdate`, `getDerivedStateFromProps` and `getSnapshotBeforeUpdate`. This are very rarely used.
6. One thing to note while writing class based components is that the config/default 				 functions should always be before the component itself as you can see in the 						 `SeasonDisplay.js` file. 
